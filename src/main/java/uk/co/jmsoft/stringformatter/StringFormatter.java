/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.stringformatter;

import java.util.Arrays;

/**
 *
 * @author Scooby
 */
public class StringFormatter
{

    public enum End
    {

        LEFT,
        RIGHT
    }

    /**
     * Returns a string of 1s and 0s representing the byte.
     * <ul>
     * <li>The string is padded with 0s at the left end to show all 8 bits.</li>
     * <li>The bits are grouped into fours and separated by "_".</li>
     * </ul>
     * <p>
     * Example output: "0100_1001".
     * <p>
     * @param aByte - The byte to be converted.
     * @return
     */
    public static String formatByteAsBinaryString(byte aByte)
    {
        return getByteAsBinaryString(aByte, 4, 8, "_", End.LEFT, End.RIGHT);
    }

    /**
     * Returns a byte as a string of binary digits.
     * <ul>
     * <li>Padded to the specified length at the specified end.</li>
     * <li>Separated at the specified chunk width by the specified string.</li>
     * <li>Separators are applied starting from the low end.</li>
     * </ul>
     * <p>
     * @param aByte           - the byte to convert.
     * @param groupSize       - how many units per group.
     * @param padLength       - how long to make the string.
     * @param separatorString - the separator to use between groups of bits.
     * @param padEnd          - the end of the string to pad with zeros.
     * @param lowEnd          - the end of the string representing the lowest
     *                        bit.
     * @return
     */
    public static String getByteAsBinaryString(byte aByte, int groupSize, int padLength, String separatorString, End padEnd, End lowEnd)
    {
        String result = Integer.toBinaryString(aByte);
        result = padToLength(result, padLength, '0', padEnd);
        return separateString(result, groupSize, lowEnd, separatorString);
    }

    /**
     * Formats a string into a leading zero binary style format by padding it with zeros to a
     * multiple of the separator width and separating it at the specified width
     * with the specified string.<br>
     * Example:<br>
     * Input: "101111", "_", 4
     * Output: "0010_1111"
     * <p>
     * @param binaryString
     * @param separator
     * @param groupSize
     * @return
     */
    public static String leadingZeroFormat(String binaryString, String separator, int groupSize)
    {
        binaryString = padToMultiplesOf(binaryString, groupSize, End.LEFT, '0');
        return separateString(binaryString, groupSize, End.RIGHT, separator);
    }

    /**
     * Pads the specified end of a string with the specified padding character 
     * so that it its length is a multiple of the specified integer.
     * @param string
     * @param multipleOf
     * @param endToPad
     * @param padWith
     * @return 
     */
    public static String padToMultiplesOf(String string, int multipleOf, End endToPad, char padWith)
    {
        StringBuilder result = new StringBuilder(string);

        for (int i = 0; i < (string.length() % multipleOf); i++)
        {
            if (endToPad == End.LEFT)
            {
                result.insert(0, padWith);
            }
            else
            {
                result.append(padWith);
            }
        }
        return result.toString();
    }
    /**
     * Pads the string to the specified length with the specified char at the specified end.
     * @param string
     * @param padToLength
     * @param paddingChar
     * @param endToPad
     * @return 
     */
    public static String padToLength(String string, int padToLength, char paddingChar, End endToPad)
    {
        String result = string;
        if (string.length() < padToLength)
        {
            char padding[] = new char[(padToLength - string.length())];
            Arrays.fill(padding, paddingChar);
            String padString = new String(padding);
            if (endToPad == End.LEFT)
            {
                result = padString + string;
            }
            else
            {
                result = string + padString;
            }
        }
        return result;
    }

    private StringFormatter()
    {
    }

    /**
     * <ul>
     * <li>
     * Separates a string into sections of a specified length separated by a
     * specified
     * separator string.</li>
     * <li>
     * Separators are only applied internally and with respect to the given
     * direction.</li>
     * </ul>
     * Examples:<br>
     * <ul>
     * <li>
     * Input string: "1234567890"<br>
     * Width: 4<br>
     * Start end: LEFT<br>
     * Separator: "_"<br>
     * Result: "1234_5678_90"<br>
     * </li>
     * <li>
     * Input string: "abcdefghij"<br>
     * Width: 3<br>
     * Start end: RIGHT<br>
     * Separator: "+"<br>
     * Result: "a+bcd+efg+hij"
     * </li>
     * </ul>
     * @param stringToSeparate The string to separate.
     * @param chunkLength      The number of characters per group.
     * @param startEnd         The end to start apply the separators from.
     * @param separator        The string to use as the separator.
     * @return
     */
    public static String separateString(String stringToSeparate, int chunkLength, End startEnd, String separator)
    {
        StringBuilder result = new StringBuilder();
        if (separator == null)
        {
            separator = "_";
        }
        if (stringToSeparate.length() > chunkLength)
        {
            if (startEnd == End.LEFT)
            {
                // Calculate how many trailing charachters there are and append them last.
                int trailinChars = stringToSeparate.length() % chunkLength;

                String trailingString = stringToSeparate.substring(stringToSeparate.length() - trailinChars, stringToSeparate.length());

                for (int i = 0; (i + chunkLength) < stringToSeparate.length(); i += chunkLength)
                {
                    result.append(stringToSeparate.substring(i, i + chunkLength)).append(separator);
                }
                result.append(trailingString);
                if (trailinChars == 0) // no trailing chars so delete final separator.
                {
                    result.deleteCharAt(result.length() - 1);
                }
            }
            else // direction.RIGHT_TO_LEFT
            {
                // Calculate how many leading charachters there are and append them first.
                int leadingChars = stringToSeparate.length() % chunkLength;
                if (leadingChars == 0) // There are no leading chars so we dont want a separator in front of the first chunk
                {
                    // add first chunk
                    result.append(stringToSeparate.substring(0, chunkLength));

                    //add central separated chunks
                    for (int i = leadingChars + chunkLength; (i + chunkLength) < stringToSeparate.length(); i += chunkLength)
                    {
                        String chunk = stringToSeparate.substring(i, i + chunkLength);
                        result.append(separator).append(chunk);
                    }
                    //add final chunk
                    result.append(separator).append(stringToSeparate.substring(stringToSeparate.length() - chunkLength, stringToSeparate.length()));

                }
                else // there are leading chars
                {
                    String leadingString = stringToSeparate.substring(0, leadingChars);
                    result.append(leadingString);
                    if (stringToSeparate.length() > leadingChars)
                    {
                        //add separated chunks
                        result.append(separator);
                        for (int i = leadingChars; (i + chunkLength) < stringToSeparate.length(); i += chunkLength)
                        {
                            String chunk = stringToSeparate.substring(i, i + chunkLength);
                            result.append(chunk).append(separator);
                        }
                        //add final string
                        result.append(stringToSeparate.substring(stringToSeparate.length() - chunkLength, stringToSeparate.length()));
                    }
                }
            }

        }
        else // String not long enough to separate
        {
            result.append(stringToSeparate);
        }
        return result.toString();
    }

    /**
     * Remove all occurrences of the specified separator from the string.
     * <p>
     * @param string
     * @param separator
     * <p>
     * @return
     */
    public static String removeSeparators(String string, String separator)
    {
        return string.replaceAll(separator, "");
    }
}
